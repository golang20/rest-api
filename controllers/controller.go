package controllers

import (
	"net/http"

	"gitlab.com/golang/rest-api/services"
)

type authorController struct{}
type bookController struct{}

var (
	authorService services.AuthorService
	bookService   services.BookService
)

// BookController interface
type BookController interface {
	Add(w http.ResponseWriter, r *http.Request)
	GetAll(w http.ResponseWriter, r *http.Request)
	GetOne(w http.ResponseWriter, r *http.Request)
	Update(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
}

// AuthorController interface
type AuthorController interface {
	Add(w http.ResponseWriter, r *http.Request)
	GetAll(w http.ResponseWriter, r *http.Request)
	GetOne(w http.ResponseWriter, r *http.Request)
}
