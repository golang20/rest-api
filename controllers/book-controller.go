package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/golang/rest-api/model"
	"gitlab.com/golang/rest-api/services"
)

// NewBookController to create concrete implementation of BookController
func NewBookController(service services.BookService) BookController {
	bookService = service
	return &bookController{}
}

func (*bookController) Add(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var book model.Book
	err := json.NewDecoder(r.Body).Decode(&book)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.Message{Msg: "error: Invalid book argument body."})
		return
	}
	err = bookService.Validate(&book)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.Message{Msg: err.Error()})
		return
	}

	id, err := bookService.Create(&book)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(model.Message{Msg: err.Error()})
		return
	}

	w.WriteHeader(http.StatusCreated)
	msg := "Book was successfully added. ID: " + strconv.Itoa(id)
	json.NewEncoder(w).Encode(model.Message{Msg: msg})
}

func (*bookController) GetAll(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	posts, err := bookService.FindAll()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(model.Message{Msg: err.Error()})
	}
	json.NewEncoder(w).Encode(posts)
}

func (*bookController) GetOne(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	id := isBookExist(w, r)
	if id == -1 {
		return
	}

	post, err := bookService.FindByID(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(model.Message{Msg: err.Error()})
		return
	}
	json.NewEncoder(w).Encode(post)
}

func (*bookController) Update(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	id := isBookExist(w, r)
	if id == -1 {
		return
	}

	var book model.Book
	err := json.NewDecoder(r.Body).Decode(&book)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.Message{Msg: "error: Invalid book argument body"})
		return
	}

	err = bookService.Validate(&book)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.Message{Msg: err.Error()})
		return
	}

	err = bookService.Update(id, &book)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(model.Message{Msg: err.Error()})
		return
	}

	json.NewEncoder(w).Encode(model.Message{Msg: "Book was successfully updated."})
}

func (*bookController) Delete(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	id := isBookExist(w, r)
	if id == -1 {
		return
	}

	err := bookService.Delete(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(model.Message{Msg: err.Error()})
		return
	}

	json.NewEncoder(w).Encode(model.Message{Msg: "Book was deleted."})
}

func isBookExist(w http.ResponseWriter, r *http.Request) int {

	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.Message{Msg: "error: Invalid value parameter for ID"})
		return -1
	}

	exist, err := bookService.Exist(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(model.Message{Msg: err.Error()})
		return -1
	}
	if !exist {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(model.Message{Msg: "error: Book not found"})
		return -1
	}

	return id
}
