package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/golang/rest-api/model"
	"gitlab.com/golang/rest-api/services"
)

// NewAuthorController to create concrete implementation of AuthorController
func NewAuthorController(service services.AuthorService) AuthorController {
	authorService = service
	return &authorController{}
}

func (*authorController) Add(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var author model.Author
	err := json.NewDecoder(r.Body).Decode(&author)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.Message{Msg: "error: Invalid author argument body."})
		return
	}
	err = authorService.Validate(&author)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.Message{Msg: err.Error()})
		return
	}

	id, err := authorService.Create(&author)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(model.Message{Msg: err.Error()})
		return
	}

	w.WriteHeader(http.StatusCreated)
	msg := "Author was successfully added. ID: " + strconv.Itoa(id)
	json.NewEncoder(w).Encode(model.Message{Msg: msg})
}

func (*authorController) GetAll(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	authors, err := authorService.FindAll()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(model.Message{Msg: err.Error()})
	}
	json.NewEncoder(w).Encode(authors)
}
func (*authorController) GetOne(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	id := isAuthorExist(w, r)
	if id == -1 {
		return
	}

	author, err := authorService.FindByID(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(model.Message{Msg: err.Error()})
	}
	json.NewEncoder(w).Encode(author)
}

func isAuthorExist(w http.ResponseWriter, r *http.Request) int {

	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.Message{Msg: "error: Invalid value parameter for ID"})
		return -1
	}

	exist, err := authorService.Exist(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(model.Message{Msg: err.Error()})
		return -1
	}
	if !exist {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(model.Message{Msg: "error: Author not found"})
		return -1
	}

	return id
}
