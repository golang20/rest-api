Go Lang Rest API and PostgreSQL (Clean Architecture)

PostgreSQL v10 - hardcoded values
username: postgres
password: password
host:port: localhost:5432
database: rest-api

TABLES - created upon start
1. Books - CREATE TABLE IF NOT EXISTS books (id SERIAL PRIMARY KEY, title TEXT, description TEXT, price int, author_id int)
2. Authors - CREATE TABLE IF NOT EXISTS authors (id SERIAL PRIMARY KEY, first_name TEXT, last_name TEXT, email TEXT)

REST APIs
Books
 POST("/books")
 GET("/books")
 GET("/books/{id}")
 PUT("/books/{id}")
 DELETE("/books/{id}")

Authors 
 POST("/authors")
 GET("/authors")
 GET("/authors/{id}")

 Dependencies
 github.com/gorilla/mux v1.8.0
 github.com/lib/pq v1.8.0


