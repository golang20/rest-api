package repository

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq" // postgres golang driver
	"gitlab.com/golang/rest-api/model"
)

type authorRepo struct{}

// NewAuthorRepository implementation of AuthorRepository
// using PQ sql library for Postgres DB
func NewAuthorRepository() AuthorRepository {

	createAuthorTable()
	return &authorRepo{}
}

func (*authorRepo) FindAll() ([]model.Author, error) {
	db := createConnection()

	defer db.Close() // close the db connection

	sqlStatement := `SELECT * FROM authors`

	rows, err := db.Query(sqlStatement)
	if err != nil {
		log.Fatalf("error: Failed to select all authors. Reason: %v", err)
		return nil, err
	}
	defer rows.Close() // close the row

	var authors []model.Author
	// iterate over the rows
	for rows.Next() {
		var author model.Author

		// unmarshal the row object to user
		err = rows.Scan(&author.ID, &author.FirstName, &author.LastName, &author.Email)
		if err != nil {
			log.Fatalf("error: Failed to scan author row. Reason %v", err)
			return nil, err
		}
		authors = append(authors, author)
	}

	return authors, nil
}
func (*authorRepo) FindByID(id int) (model.Author, error) {

	db := createConnection()
	// close the db connection
	defer db.Close()

	sqlStatement := `SELECT * FROM authors WHERE id=$1`

	// execute the sql statement
	row := db.QueryRow(sqlStatement, id)

	var author model.Author
	err := row.Scan(&author.ID, &author.FirstName, &author.LastName, &author.Email)

	switch err {
	case sql.ErrNoRows:
		return model.Author{}, nil
	case nil:
		return author, nil
	default:
		log.Fatalf("error: Failed to scan author row. %v", err)
		return model.Author{}, err
	}
}

func (*authorRepo) Create(author *model.Author) (int, error) {

	db := createConnection()
	// close the db connection
	defer db.Close()

	var id int
	sqlStatement := `INSERT INTO authors (first_name, last_name, email) VALUES ($1, $2, $3) RETURNING id`
	err := db.QueryRow(sqlStatement, author.FirstName, author.LastName, author.Email).Scan(&id)
	if err != nil {
		log.Fatalf("error: Failed to insert author item. Reason: %v", err)
		return 0, err
	}
	return id, nil
}

func (*authorRepo) Exist(id int) (bool, error) {

	db := createConnection()
	// close the db connection
	defer db.Close()

	sqlStatement := `SELECT id FROM authors WHERE id=$1`

	row := db.QueryRow(sqlStatement, id)

	var author model.Author

	err := row.Scan(&author.ID)
	switch err {
	case sql.ErrNoRows:
		return false, nil
	case nil:
		return true, nil
	default:
		log.Fatalf("error: Failed to scan author row. %v", err)
		return false, err
	}
}
