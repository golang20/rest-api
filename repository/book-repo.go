package repository

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq" // postgres golang driver
	"gitlab.com/golang/rest-api/model"
)

type bookRepo struct{}

// NewBookRepository implementation of BookRepository
// using PQ sql library for Postgres DB
func NewBookRepository() BookRepository {

	createBookTable()
	return &bookRepo{}
}

func (*bookRepo) FindAll() ([]model.Book, error) {
	db := createConnection()

	defer db.Close() // close the db connection

	sqlStatement := `SELECT b.id, title, description, price, a.id, first_name, last_name, email FROM books b JOIN authors a ON b.author_id = a.id`

	rows, err := db.Query(sqlStatement)
	if err != nil {
		log.Fatalf("error: Failed to select all books. Reason: %v", err)
		return nil, err
	}
	defer rows.Close() // close the row

	var books []model.Book
	// iterate over the rows
	for rows.Next() {
		var book model.Book

		// unmarshal the row object to user
		err := rows.Scan(&book.ID, &book.Title, &book.Description, &book.Price,
			&book.Author.ID, &book.Author.FirstName, &book.Author.LastName, &book.Author.Email)
		if err != nil {
			log.Fatalf("error: Failed to scan book row. Reason %v", err)
			return nil, err
		}
		books = append(books, book)
	}

	return books, nil
}

func (*bookRepo) FindByID(id int) (model.Book, error) {

	db := createConnection()
	// close the db connection
	defer db.Close()

	sqlStatement := `SELECT b.id, title, description, price, a.id, first_name, last_name, email FROM books b JOIN authors a ON b.author_id = a.id WHERE b.id=$1`

	// execute the sql statement
	row := db.QueryRow(sqlStatement, id)

	var book model.Book
	err := row.Scan(&book.ID, &book.Title, &book.Description, &book.Price,
		&book.Author.ID, &book.Author.FirstName, &book.Author.LastName, &book.Author.Email)

	switch err {
	case sql.ErrNoRows:
		return model.Book{}, nil
	case nil:
		return book, nil
	default:
		log.Fatalf("error: Failed to scan book row. %v", err)
		return model.Book{}, err
	}
}
func (*bookRepo) Create(book *model.Book) (int, error) {

	db := createConnection()

	// close the db connection
	defer db.Close()

	var id int
	sqlStatement := `INSERT INTO books (title, description, price, author_id) VALUES ($1, $2, $3, $4) RETURNING id`
	err := db.QueryRow(sqlStatement, book.Title, book.Description, book.Price, book.Author.ID).Scan(&id)
	if err != nil {
		log.Fatalf("error: Failed to insert book item. Reason: %v", err)
		return 0, err
	}
	return id, nil
}
func (*bookRepo) Update(id int, book *model.Book) error {

	db := createConnection()
	// close the db connection
	defer db.Close()

	sqlStatement := `UPDATE books SET title=$2, description=$3, price=$4, author_id=$5 WHERE id=$1`

	res, err := db.Exec(sqlStatement, id, book.Title, book.Description, book.Price, book.Author.ID)
	if err != nil {
		log.Fatalf("error: Failed to update book item. Reason: %v", err)
		return err
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		log.Fatalf("error: Failed to check affected rows. %v", err)
	}

	fmt.Printf("Total rows/record affected %v", rowsAffected)
	return nil
}
func (*bookRepo) Delete(id int) error {
	db := createConnection()
	// close the db connection
	defer db.Close()

	sqlStatement := `DELETE FROM books WHERE id=$1`

	res, err := db.Exec(sqlStatement, id)
	if err != nil {
		log.Fatalf("error: Failed to delete book item. Reason: %v", err)
		return err
	}

	// check how many rows affected
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		log.Fatalf("error: Failed to check affected rows. %v", err)
		return err
	}

	fmt.Printf("Total rows/record affected %v", rowsAffected)
	return nil
}
func (*bookRepo) Exist(id int) (bool, error) {
	db := createConnection()
	// close the db connection
	defer db.Close()

	sqlStatement := `SELECT id FROM books WHERE id=$1`

	row := db.QueryRow(sqlStatement, id)

	var book model.Book

	err := row.Scan(&book.ID)
	switch err {
	case sql.ErrNoRows:
		return false, nil
	case nil:
		return true, nil
	default:
		log.Fatalf("error: Failed to scan book row. %v", err)
		return false, err
	}
}
