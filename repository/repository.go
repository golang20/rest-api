package repository

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq" // postgres golang driver
	"gitlab.com/golang/rest-api/model"
)

// BookRepository interface
type BookRepository interface {
	FindAll() ([]model.Book, error)
	FindByID(id int) (model.Book, error)
	Create(book *model.Book) (int, error)
	Update(id int, book *model.Book) error
	Delete(id int) error
	Exist(id int) (bool, error)
}

// AuthorRepository interface
type AuthorRepository interface {
	FindAll() ([]model.Author, error)
	FindByID(id int) (model.Author, error)
	Create(author *model.Author) (int, error)
	Exist(id int) (bool, error)
}

func createConnection() *sql.DB {

	// Open the connection
	connStr := "user=postgres password=password dbname=rest-api sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatalf("error: Failed to connect to DB. Reason: %v\n", err)
	}

	// check the connection
	err = db.Ping()
	if err != nil {
		log.Fatalf("error: Failed to check DB connection. Reason: %v\n", err)
	}

	log.Print("Successfully connected to DB.")

	return db
}
func createBookTable() {

	var sql = "CREATE TABLE IF NOT EXISTS books (id SERIAL PRIMARY KEY, "
	sql += "title TEXT, "
	sql += "description TEXT, "
	sql += "price int, "
	sql += "author_id int)"

	db := createConnection()

	// close the db connection
	defer db.Close()

	_, err := db.Exec(sql)
	if err != nil {
		log.Fatalf("error: Failed to create table Books. Reason: %v\n", err)
	}
	log.Print("Successfully created table Books.")
}

func createAuthorTable() {

	var sql = "CREATE TABLE IF NOT EXISTS authors (id SERIAL PRIMARY KEY, "
	sql += "first_name TEXT, "
	sql += "last_name TEXT, "
	sql += "email TEXT)"

	db := createConnection()

	// close the db connection
	defer db.Close()

	_, err := db.Exec(sql)
	if err != nil {
		log.Fatalf("error: Failed to create table Authors. Reason: %v\n", err)
	}
	log.Print("Successfully created table Authors.")
}
