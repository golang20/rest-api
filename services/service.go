package services

import (
	"gitlab.com/golang/rest-api/model"
	"gitlab.com/golang/rest-api/repository"
)

type authorService struct{}
type bookService struct{}

var (
	books      []model.Book = []model.Book{}
	bookRepo   repository.BookRepository
	authorRepo repository.AuthorRepository
)

// AuthorService interface
type AuthorService interface {
	Create(author *model.Author) (int, error)
	FindAll() ([]model.Author, error)
	FindByID(id int) (model.Author, error)
	Validate(author *model.Author) error
	Exist(id int) (bool, error)
}

// BookService interface
type BookService interface {
	Create(book *model.Book) (int, error)
	FindAll() ([]model.Book, error)
	FindByID(id int) (model.Book, error)
	Update(id int, book *model.Book) error
	Delete(id int) error
	Validate(book *model.Book) error
	Exist(id int) (bool, error)
}
