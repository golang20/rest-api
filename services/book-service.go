package services

import (
	"errors"
	"strings"

	"gitlab.com/golang/rest-api/model"
	"gitlab.com/golang/rest-api/repository"
)

// NewBookService concrete implementation BookService
func NewBookService(br repository.BookRepository, ar repository.AuthorRepository) BookService {
	bookRepo = br
	authorRepo = ar
	return &bookService{}
}

func (*bookService) Create(book *model.Book) (int, error) {

	id, err := bookRepo.Create(book)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (*bookService) FindAll() ([]model.Book, error) {

	books, err := bookRepo.FindAll()
	if err != nil {
		return nil, err
	}
	return books, nil
}

func (*bookService) FindByID(id int) (model.Book, error) {

	book, err := bookRepo.FindByID(id)
	if err != nil {
		return model.Book{}, err
	}
	return book, nil
}
func (bs *bookService) Update(id int, book *model.Book) error {

	err := bookRepo.Update(id, book)
	if err != nil {
		return err
	}

	return nil
}

func (*bookService) Delete(id int) error {

	err := bookRepo.Delete(id)
	if err != nil {
		return err
	}
	return nil
}

func (*bookService) Validate(book *model.Book) error {
	if book == nil {
		err := errors.New("error: Book is empty")
		return err
	}
	if strings.TrimSpace(book.Title) == "" {
		err := errors.New("error: Title is empty")
		return err
	}
	if strings.TrimSpace(book.Description) == "" {
		err := errors.New("error: Description is empty")
		return err
	}
	if book.Price == 0 {
		err := errors.New("error: Price must be greater than 0")
		return err
	}

	exist, err := authorRepo.Exist(book.Author.ID)
	if err != nil {
		return err
	}
	if !exist {
		err := errors.New("error: Author does not exist")
		return err
	}
	return nil
}

func (*bookService) Exist(id int) (bool, error) {
	exist, err := bookRepo.Exist(id)
	if err != nil {
		return false, err
	}
	return exist, nil
}
