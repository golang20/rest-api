package services

import (
	"errors"
	"strings"

	"gitlab.com/golang/rest-api/model"
	"gitlab.com/golang/rest-api/repository"
)

// NewAuthorService concrete implementation AuthorService
func NewAuthorService(ar repository.AuthorRepository) AuthorService {
	authorRepo = ar
	return &authorService{}
}

func (*authorService) Create(author *model.Author) (int, error) {

	id, err := authorRepo.Create(author)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (*authorService) FindAll() ([]model.Author, error) {

	authors, err := authorRepo.FindAll()
	if err != nil {
		return nil, err
	}
	return authors, nil
}
func (*authorService) FindByID(id int) (model.Author, error) {

	author, err := authorRepo.FindByID(id)
	if err != nil {
		return model.Author{}, err
	}
	return author, nil
}

func (*authorService) Validate(author *model.Author) error {

	if strings.TrimSpace(author.FirstName) == "" {
		err := errors.New("error: Author's First name is empty")
		return err
	}
	if strings.TrimSpace(author.LastName) == "" {
		err := errors.New("error: Author's Last name is empty")
		return err
	}
	if strings.TrimSpace(author.Email) == "" {
		err := errors.New("error: Author's Email is empty")
		return err
	}
	return nil
}

func (*authorService) Exist(id int) (bool, error) {

	exist, err := authorRepo.Exist(id)
	if err != nil {
		return false, err
	}
	return exist, nil
}
