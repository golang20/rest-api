package model

// Book model
type Book struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Price       int    `json:"price"`
	Author      Author `json:"author"`
}

// Author model
type Author struct {
	ID        int    `json:"id"`
	FirstName string `json:"firstName"`
	LastName  string `json:"LastName"`
	Email     string `json:"email"`
}

// Message model
type Message struct {
	Msg string `json:"msg"`
}
