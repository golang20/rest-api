package router

import "net/http"

// Router interface that will be used to create a concrete implementation of web server.
type Router interface {
	GET(uri string, f func(w http.ResponseWriter, r *http.Request))
	POST(uri string, f func(w http.ResponseWriter, r *http.Request))
	PUT(uri string, f func(w http.ResponseWriter, r *http.Request))
	DELETE(uri string, f func(w http.ResponseWriter, r *http.Request))
	PATCH(uri string, f func(w http.ResponseWriter, r *http.Request))
	SERVE(port string)
}
