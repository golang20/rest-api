package router

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type muxRouter struct{}

var (
	router = mux.NewRouter()
)

// NewMuxRouter is a Router implementation for Gorilla/mux web server
func NewMuxRouter() Router {
	return &muxRouter{}
}

func (*muxRouter) GET(uri string, f func(w http.ResponseWriter, r *http.Request)) {
	router.HandleFunc(uri, f).Methods("GET")
}
func (*muxRouter) POST(uri string, f func(w http.ResponseWriter, r *http.Request)) {
	router.HandleFunc(uri, f).Methods("POST")
}
func (*muxRouter) PUT(uri string, f func(w http.ResponseWriter, r *http.Request)) {
	router.HandleFunc(uri, f).Methods("PUT")
}
func (*muxRouter) DELETE(uri string, f func(w http.ResponseWriter, r *http.Request)) {
	router.HandleFunc(uri, f).Methods("DELETE")
}
func (*muxRouter) PATCH(uri string, f func(w http.ResponseWriter, r *http.Request)) {
	router.HandleFunc(uri, f).Methods("PATCH")
}
func (*muxRouter) SERVE(port string) {
	log.Printf("MUX HTTP Server running on port %v", port)
	log.Fatal(http.ListenAndServe(":"+port, router))
}
