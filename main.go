package main

import (
	"os"

	"gitlab.com/golang/rest-api/controllers"
	"gitlab.com/golang/rest-api/repository"
	"gitlab.com/golang/rest-api/router"
	"gitlab.com/golang/rest-api/services"
)

var (
	httpRouter router.Router = router.NewMuxRouter()

	bookRepo   repository.BookRepository   = repository.NewBookRepository()
	authorRepo repository.AuthorRepository = repository.NewAuthorRepository()

	postService   services.BookService   = services.NewBookService(bookRepo, authorRepo)
	authorService services.AuthorService = services.NewAuthorService(authorRepo)

	bookCon   controllers.BookController   = controllers.NewBookController(postService)
	authorCon controllers.AuthorController = controllers.NewAuthorController(authorService)
)

const defaultPort = "5000"

// go build | .\rest-api
func main() {

	httpRouter.POST("/books", bookCon.Add)
	httpRouter.GET("/books", bookCon.GetAll)
	httpRouter.GET("/books/{id}", bookCon.GetOne)
	httpRouter.PUT("/books/{id}", bookCon.Update)
	httpRouter.DELETE("/books/{id}", bookCon.Delete)

	httpRouter.POST("/authors", authorCon.Add)
	httpRouter.GET("/authors", authorCon.GetAll)
	httpRouter.GET("/authors/{id}", authorCon.GetOne)

	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}
	httpRouter.SERVE(port)
}
