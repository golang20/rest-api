package entity

import (
	"log"
	"os"
	"time"

	pg "github.com/go-pg/pg"
	orm "github.com/go-pg/pg/orm"
)

// Author entity
type Author struct {
	tableName struct{}  `sql:"authors"`
	ID        int       `sql:"id,pk"`
	FirstName string    `sql:"first_name"`
	LastName  string    `sql:"last_name"`
	Email     string    `sql:"email"`
	CreatedAt time.Time `sql:"created_at"`
	UpdatedAt time.Time `sql:"updated_at"`
}

// CreateAuthorTable method for creating Author table
func CreateAuthorTable(db *pg.DB) {
	opts := &orm.CreateTableOptions{
		IfNotExists: true,
	}
	createErr := db.CreateTable(&Author{}, opts)
	if createErr != nil {
		log.Printf("Error in creating Author table. Reason: %v\n", createErr)
		os.Exit(100)
	}
	log.Printf("Table Author successfully created.")
}
