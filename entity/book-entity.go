package entity

import (
	"log"
	"os"
	"time"

	pg "github.com/go-pg/pg"
	orm "github.com/go-pg/pg/orm"
)

// Book entity
type Book struct {
	tableName   struct{}  `sql:"books"`
	ID          int       `sql:"id,pk"`
	Title       string    `sql:"title"`
	Description string    `sql:"description"`
	Price       float64   `sql:"type:real"`
	AuthorID    int       `sql:"author_id,fk:Author"`
	CreatedAt   time.Time `sql:"created_at"`
	UpdatedAt   time.Time `sql:"updated_at"`
}

// CreateBookTable method for creating Book table
func CreateBookTable(db *pg.DB) {
	opts := &orm.CreateTableOptions{
		IfNotExists: true,
	}
	createErr := db.CreateTable(&Book{}, opts)
	if createErr != nil {
		log.Printf("Error in creating Book table. Reason: %v\n", createErr)
		os.Exit(100)
	}
	log.Printf("Table Book successfully created.")

}
